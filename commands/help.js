const Discord = require('discord.js');

module.exports = {
    name: 'help',
    description: 'help',
    async exec(msg, args) {
        let embed = new Discord.RichEmbed()
        embed.setColor(0x9d1005)
        .setTitle('commands')
        .setAuthor('AniGuess', 'https://imgur.com/a/Yrw7rsh')
        .addField('Anime Songs', '-----------------')
        .addField(`command`, '!music X Y', true)
        .addField(`description`, 'start guessing game with\nX songs which are played\nY seconds each', true)
        .addField('aliases', 'todo', true)
        .addField(`\u200b`, '\u200b')
        .addField('guessCharacter', '-----------------')
        .addField(`command`, `!gc X Y`, true)
        .addField(`description`, `start a game with X characters\n& Y sec guess time`, true)
        .addField(`aliases`, 'todo', true)
        .addField(`\u200b`, '\u200b')
        .addField(`\u200b`, `!gc AL1 AL2 X Y`, true)
        .addField(`\u200b`, `start a game based on\nAL1 ∩ AL2 with:\nX characters\n& Y sec guess time`, true)
        .addField(`\u200b`, `todo`, true)
        .addField(`\u200b`, `\u200b`)
        .addField('Search', '-----------------')
        .addField('command', '!search Anime', true)
        .addField('description', 'search for Aime', true)
        .addField('aliases', 'todo', true)
        .addField('\u200b', '\u200b', false)
        .addField('\u200b', '!search c __', true)
        .addField('\u200b', 'search for character __', true)
        .addField('\u200b', 'todo', true)

        .addBlankField()
        .setFooter('Author: a dwarf named Nils');

        msg.author.send(embed);
        msg.react('✅');
    }
}