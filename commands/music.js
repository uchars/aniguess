/*      ___     ___                          __       
      //   \\ //  \\      ||      ||        \_/     ||=======
    ||     \\//    \\     ||      ||        ||      ||
    ||      ||      ||   ||      ||     S   ||      ||
    ||      ||      ||   ||      ||         ||      ||
    ||      ||      ||   ||||||||||         ||      ||=======
    McDonald's M

Author: a dwarf named Nils (_sonsan)
*/
const yt = require('ytdl-core');
const songs = require('../data/songs.json');
const { gmActive, gmSetActive, gmSetInactive } = require('../handlers/dbHandler');
const { logger } = require('../handlers/logHandler');

async function shuffle(arr) {
    /*
    * returns the suffled the array.
    * NOTE: maybe use https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    */
    for (let i = arr.length -1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i+1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}

async function game(msg, conn, server, round, gT) {
    const filter = resp => server.song_queue[0].answers.some(a => a.toLowerCase() === resp.content.toLowerCase());
    logger.info(`starting ${server.song_queue[0].song_link} in server: ${msg.guild.id}`);

    server.dispatcher = await conn.playStream(yt(server.song_queue[0].song_link, {filter: 'audioonly'} ));
    msg.channel.awaitMessages(filter, {maxMatches: 1, time: gT*1000})
                .then(c => {
                    c.first().reply('knew');
                    server.dispatcher.end();})
                .catch(e => {
                    msg.channel.send(`no one guessed the right answer. It would have been:\n **${server.song_queue[0].answers[0]}**`)
                    server.dispatcher.end();});
    
    /*
    * when either a player knew the right answer or the song enden
    *   /_ go to the next song, if there are still round's left and no exit command was provided
    */
    server.dispatcher.on('end', async () => {
        server.song_queue.shift();
        if (!round || !await gmActive(msg)) {
            msg.guild.voiceConnection.disconnect();
            await gmSetInactive(msg);
        } else {
            game(msg, conn, server, round-1, gT);
        }
    });
}

module.exports = {
    name: 'music',
    aliases: ['play', 'p'],
    args: true,
    description: 'guess anime openings/endings',
    async exec(msg, args, servers) {
        if (!msg.guild.voiceConnection && msg.member.voiceChannel) {
            servers[msg.guild.id] = {song_queue: []}
            await msg.channel.send(`Starting the Anime Song quiz with **${Math.abs(args[0])} rounds** and **${Math.abs(args[1])}s playtime**`);
            const arr = songs.filter(s => s['type'] === 'OPENING')
            servers[msg.guild.id].song_queue = await shuffle(arr);
            msg.member.voiceChannel.join().then(async function(conn) {
                /*
                * if the provided number of songs is greater than the aviable song list
                * change it accordingly.
                */
                args[0] > arr.length ? args[0] = arr.length : args[0]
                await gmSetActive(msg);
                await game(msg, conn, servers[msg.guild.id], Math.abs(parseInt(args[0])-1), Math.abs(parseInt(args[1])));
            });
        } else{
            msg.reply(`you are either not in a voice channel or in one i can't access.`);
        }
    }
}
