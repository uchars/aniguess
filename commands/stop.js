// TODO: Maybe also do servers[msg.guild.id].dispatcher.end();
const { gmSetInactive } = require('../handlers/dbHandler');

module.exports = {
    name: 'stop',
    description: 'stops music',
    exec(msg, args, servers) {
        if (msg.guild.voiceConnection) {
            msg.guild.voiceConnection.disconnect();
            gmSetInactive(msg);
        } else {
            msg.channel.send(`***no game in progress!***`);
        }
    }
}