const { GraphQLClient } = require('graphql-request');
const { logger } = require('./logHandler');

const client = new GraphQLClient('https://graphql.anilist.co', {redirect: 'follow'});
const fetch = (query, args) => client.request(query, args).then(data => data).catch(e => {
    logger.warn(e);
    return false;
});

module.exports = fetch;
