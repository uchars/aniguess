/*
*   https://github.com/winstonjs/winston
*/
const {createLogger, format, transports} = require('winston');
const fs = require('fs');
const path = require('path');

if (!fs.existsSync('log')) fs.mkdirSync('log');
const combFN = path.join('log', 'combined.log')
const warnFN = path.join('log', 'warn.log');
const errorFN = path.join('log', 'error.log');

const logger = createLogger({
    level: 'info',
    format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss'}),
        format.printf(i => `[${i.level.toUpperCase()}](${i.timestamp}) ${i.message}`)
    ),
    transports: [
        new transports.File( {filename: combFN} ),
        new transports.File( {filename: warnFN, level: 'warn'}),
        new transports.File( {filename: errorFN, level: 'error'})
    ]
});

module.exports = {
    logger
}