
/* source: https://discordjs.guide/sequelize/#installing-and-using-sequelize
*       https://sequelize.org/master/manual/getting-started
*/
const Sequelize = require('sequelize');
const config = require('../config.json');

// set information about the connection
const sequelize = new Sequelize(config.dbname, config.dbuser, config.dbpassword, {
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    operatorAliases: false,
    storage: config.dbstorage,
});

// define database structures
const Server = sequelize.define('server', {
    serverID:  { type: Sequelize.STRING, unique: true, primaryKey: true },
    gcActive:  { type: Sequelize.BOOLEAN, defaultValue: false, allowNull: false },
    gmActive:  { type: Sequelize.BOOLEAN, defaultValue: false, allowNull: false },
    userCount: { type: Sequelize.INTEGER, defaultValue: 2, allowNull: false }
});

const User = sequelize.define('user', {
    userID: { type: Sequelize.STRING, unique: true, primaryKey: true },
    gcWon:  { type: Sequelize.INTEGER, defaultValue: 0 },
    gmWon:  { type: Sequelize.INTEGER, defaultValue: 0 }
});

const addServer = async (guild) => await Server.create({ serverID: guild.id, userCount: guild.memberCount});

const userJoin = async (member) => {
    const oldCount = await Server.findOne ({ where: {serverID: member.guild.id } });
    await Server.update({ userCount: oldCount.userCount+1 }, { where: {serverID: member.guild.id } });
};

const userLeave = async (member) => {
    const oldCount = await Server.findOne ({ where: {serverID: member.guild.id } });
    await Server.update({ userCount: oldCount.userCount-1 }, { where: {serverID: member.guild.id } });
};

const gcActive = async (msg) => {
    const server = await Server.findOne({ where: {serverID: msg.guild.id } });
    if (server) return server.gcActive;
};

const gmActive = async (msg) => {
    const server = await Server.findOne({ where: {serverID: msg.guild.id } });
    if (server) return server.gmActive;
};

const gcSetActive = async msg => await Server.update({ gcActive: true }, { where: { serverID: msg.guild.id }});
const gcSetInactive = async msg => await Server.update({ gcActive: false }, { where: { serverID: msg.guild.id }});

const gmSetActive = async msg => await Server.update({ gmActive: true }, { where: { serverID: msg.guild.id }});
const gmSetInactive = async msg => await Server.update({ gmActive: false }, { where: { serverID: msg.guild.id }});


module.exports = {
    Server, User,
    addServer, userJoin, userLeave,
    gcActive, gmActive,
    gcSetActive, gmSetActive,
    gcSetInactive, gmSetInactive
}